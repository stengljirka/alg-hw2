package alg;

public class StartingPositionsHolder {
    StartingPosition squareStrategy, parcelStrategy;

    public StartingPositionsHolder(StartingPosition squareStrategy, StartingPosition parcelStrategy) {
        this.squareStrategy = squareStrategy;
        this.parcelStrategy = parcelStrategy;
    }

    public boolean areSame(){
        return this.squareStrategy.value == this.parcelStrategy.value;
    }


}
