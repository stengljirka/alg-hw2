package alg;

import java.lang.reflect.Array;
import java.util.Arrays;

public class CottagePrices {
    private int width, height;
    private int prices[];
    public CottagePrices( PricesParcel pricesParcel, CottageParcel parcel){
        this.width = pricesParcel.getWidth();
        this.height = pricesParcel.getHeight();
        prices = new int[width * height];
        for (int y = 0; y < this.height; y++){
            for (int x = 0; x < this.width; x++){
                prices[x + y * width] = getValueOfCottageParcel(x, y, pricesParcel, parcel);
            }
        }
    }
    private int getValueOfCottageParcel(int startX, int startY, PricesParcel pricesParcel, CottageParcel parcel) {
        if (startX + parcel.getWidth() > pricesParcel.getWidth() || startY + parcel.getHeight() > pricesParcel.getHeight()) {
            return Integer.MIN_VALUE;
        }
        int possibleValue = 0;
        for (int x = startX; x < startX + parcel.getWidth(); x++) {
            for (int y = startY; y < startY + parcel.getHeight(); y++) {
                possibleValue += pricesParcel.prices[x][y];
            }
        }
        return possibleValue;
    }
    public int getPrice(int x, int y){
        return this.prices[x + y * width];
    }

    @Override
    public String toString() {
        return "CottagePrices{" +
                "width=" + width +
                ", height=" + height +
                ", prices=" + Arrays.toString(prices) +
                '}';
    }
}
