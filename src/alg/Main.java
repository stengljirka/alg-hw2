package alg;

import java.io.IOException;
import java.util.*;

public class Main {
    static int maxSize = 0;
    static int counter = 0;
    static int singleCounter = 0;
    static int biggestCounter = 0;
    static int smallestWidth = Integer.MAX_VALUE;
    final static int HIGHEST_PARCEL_VALUE_STRATEGY = 0;
    final static int HIGHEST_SQUARE_VALUE_STRATEGY = 1;
    private static CottagePrices[] cottagePrices;
    private static CottageParcel[] cottages;
    private static Map<String, Integer> usedStrategies;

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        InputReader.init();
        PricesParcel parcel = InputReader.readPricesParcel();
        cottages = InputReader.readCottageParcels();
        Arrays.sort(cottages);
        InputReader.close();
        usedStrategies = new HashMap<>();
        cottagePrices = new CottagePrices[cottages.length];
        for (int i = 0; i < cottages.length; i++) {
            cottagePrices[i] = new CottagePrices(parcel, cottages[i]);
            smallestWidth = Math.min(cottages[i].getWidth(), smallestWidth);
        }

        int maxArea = cottages[0].getHeight() * cottages[0].getWidth();
        for (int i = 0; i < cottages.length; i++) {
            if (maxArea > cottages[i].getHeight() * cottages[i].getWidth()) {
                break;
            }
            CalculationData calculationData = new CalculationData(cottages, parcel.getWidth(), parcel.getHeight());
            calculationData.startingPosition = tryToFitInCottage(calculationData, parcel, i, HIGHEST_SQUARE_VALUE_STRATEGY);
            calculateMaxValue(calculationData, i, parcel);
        }
        System.out.println(maxSize);
        System.err.println(System.currentTimeMillis() - start);
        System.err.println(counter);
        System.err.println(biggestCounter);
        System.err.println(singleCounter);
        System.err.println(smallestWidth);
    }

    private static void calculateMaxValue(CalculationData calculationData, int parcelIndex, PricesParcel parcel) {
        tryToFitParcelInCoords(calculationData.startingPosition, calculationData, parcelIndex, parcel);
        calculationData.cottagesReps[parcelIndex]--;
        maxSize = Math.max(calculationData.maxSize, maxSize);
        counter++;
        String key = Arrays.toString(calculationData.cottagesReps);
        Integer oldStrategy = usedStrategies.get(key);
        if (oldStrategy == null) {
            usedStrategies.put(key, calculationData.maxSize);
            createNextGeneration(calculationData, parcel);
        } else {
            if (oldStrategy <= calculationData.maxSize){
                //usedStrategies.put(key, calculationData.maxSize);
                createNextGeneration(calculationData, parcel);
                singleCounter++;
            }
        }
    }

    private static void createNextGeneration(CalculationData calculationData, PricesParcel parcel) {
        int minIndex = -1;
        int counter = 0;
        StartingPositionsHolder[] positions = new StartingPositionsHolder[cottages.length];
        for (int i = 0; i < calculationData.cottagesReps.length; i++) {
            if (calculationData.cottagesReps[i] > 0) {
                StartingPosition newStartingPosition = tryToFitInCottage(calculationData, parcel, i, HIGHEST_SQUARE_VALUE_STRATEGY);
                if (newStartingPosition.value > 0) {
                    counter++;
                    StartingPosition secondStrategy = tryToFitInCottage(calculationData, parcel, i, HIGHEST_PARCEL_VALUE_STRATEGY);
                    minIndex = i;
                    positions[i] = new StartingPositionsHolder(newStartingPosition, secondStrategy);
                } else {
                    calculationData.cottagesReps[i] = 0;
                }
            }
        }
        if (counter == 0) return;
        else if (counter == 1) {
            if (positions[minIndex].areSame()) {
                calculationData.startingPosition = positions[minIndex].parcelStrategy;
                calculateMaxValue(calculationData, minIndex, parcel);
            } else {
                calculateMaxValue(new CalculationData(calculationData, positions[minIndex].squareStrategy), minIndex, parcel);
                calculationData.startingPosition = positions[minIndex].parcelStrategy;
                calculateMaxValue(calculationData, minIndex, parcel);
            }
            return;
        }

        for (int i = 0; i < calculationData.cottagesReps.length; i++) {
            if (positions[i] != null) {
                if (i == minIndex) {
                    if (positions[minIndex].areSame()) {
                        calculationData.startingPosition = positions[minIndex].parcelStrategy;
                        calculateMaxValue(calculationData, minIndex, parcel);
                    } else {
                        calculateMaxValue(new CalculationData(calculationData, positions[minIndex].squareStrategy), minIndex, parcel);
                        calculationData.startingPosition = positions[minIndex].parcelStrategy;
                        calculateMaxValue(calculationData, minIndex, parcel);
                    }
                    break;
                }
                calculateMaxValue(new CalculationData(calculationData, positions[i].squareStrategy), i, parcel);
                if (!positions[i].areSame()) {
                    calculateMaxValue(new CalculationData(calculationData, positions[i].parcelStrategy), i, parcel);
                }
            }
        }
    }

    private static StartingPosition tryToFitInCottage(CalculationData data, PricesParcel parcel, int parcelIndex, int strategy) {
        StartingPosition start = new StartingPosition(-1, -1, -1);
        for (int y = data.startY; y < parcel.getHeight() - cottages[parcelIndex].getHeight() + 1; y++) {
            for (int x = data.startingXPositions[y]; x < parcel.getWidth() - cottages[parcelIndex].getWidth() + 1; x++) {
                if (data.unusedBlocks[x + y * data.width]) {
                    int value = cottagePrices[parcelIndex].getPrice(x, y);
                    if (value > start.value && isItPlaceableHere(x, y, data, parcelIndex)) {
                        start.y = y;
                        start.x = x;
                        start.value = value;
                        break;
                    } else if (value < start.value || value < 0) {
                        break;
                    }
                }
                if (strategy == HIGHEST_SQUARE_VALUE_STRATEGY && start.value > 0) {
                    return start;
                }
            }
        }
        return start;
    }

    private static boolean isItPlaceableHere(int xStart, int yStart, CalculationData data, int parcelIndex) {
        if (cottagePrices[parcelIndex].getPrice(xStart, yStart) < 0) return false;
        for (int x = xStart; x < xStart + cottages[parcelIndex].getWidth(); x++) {
            for (int y = yStart; y < yStart + cottages[parcelIndex].getHeight(); y++) {
                if (!data.unusedBlocks[x + y * data.width]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean tryToFitParcelInCoords(StartingPosition start, CalculationData data, int parcelIndex, PricesParcel pricesParcel) {
        CottageParcel parcel = cottages[parcelIndex];
        int startX = Math.max(start.x - 1, 0);
        int endX = Math.min(data.width - 1, start.x + parcel.getWidth());
        for (int y = start.y - 1; y < start.y + parcel.getHeight() + 1; y++) {
            if (y < 0) continue;
            if (y > data.height - 1) break;
            Arrays.fill(data.unusedBlocks, startX + y * data.width, endX + y * data.width + 1, false);
            data.startingXPositions[y] = Math.max(data.startingXPositions[y], start.x + parcel.getWidth());
            int trues = 0;
            for (int x = 0; x < data.width; x++) {
                trues = data.unusedBlocks[x + y * data.width] ? 1 : 0;
            }
            data.startY = trues == 0 ? y : data.startY;
        }

        data.maxSize += start.value;
        return true;
    }

}
