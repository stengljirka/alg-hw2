package alg;

public class StartingPosition implements Comparable<StartingPosition>{
    int x, y, value;
    @Override
    public int compareTo(StartingPosition o) {
        if (this.value > o.value) return -1;
        else if (this.value == o.value) return 0;
        return 1;
    }

    @Override
    public String toString() {
        return "StartingPosition{" +
                "x=" + x +
                ", y=" + y +
                ", value=" + value +
                '}';
    }

    public StartingPosition(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
}
