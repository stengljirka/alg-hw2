package alg;

import java.util.ArrayList;
import java.util.Arrays;

public class PricesParcel {
    private final int width, height;
    int prices[][];
    private int sum = -1;
    int linePrices [][];

    public PricesParcel(int width, int height) {
        this.width = width;
        this.height = height;
        prices = new int[width][height];
        linePrices = new int[width][height];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getSum(){
        if (sum == -1){
            sum = 0;
            for (int x = 0; x < this.width; x++){
                for (int y = 0; y < this.height; y++){
                    sum += this.prices[x][y];
                }
            }
        }
        return sum;
    }

    @Override
    public String toString() {
        String message = "PricesParcel{" +
                "width=" + width +
                ", height=" + height +
                ", prices=\n";

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                message += this.prices[x][y] + " ";
            }
            message += "\n";
        }

        return message;
    }
}
