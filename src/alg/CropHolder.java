package alg;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CropHolder {
    int maxPrice;
    Set<String> paths;

    public CropHolder(int maxPrice, String path) {
        paths = new HashSet<>();
        paths.add(path);
        this.maxPrice = maxPrice;
    }
}
