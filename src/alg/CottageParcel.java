package alg;

public class CottageParcel implements Comparable<CottageParcel>{
    private final int width, height;
    int repetitons;

    @Override
    public String toString() {
        return "CottageParcel{" +
                "width=" + width +
                ", height=" + height +
                ", repetitons=" + repetitons +
                '}';
    }

    public CottageParcel(int width, int height, int repetitons) {
        this.width = width;
        this.height = height;
        this.repetitons = repetitons;
    }

    public CottageParcel(CottageParcel cottage) {
        this.width = cottage.width;
        this.height = cottage.height;
        this.repetitons = cottage.repetitons;
    }
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    @Override
    public int compareTo(CottageParcel o) {
        if (this.height * this.width > o.height * o.width) return -1;
        else if (this.height * this.width == o.height * o.width) return 0;
        else return 1;
    }
}
