package alg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class InputReader {
    private static final int HEIGHT_INDEX = 0;
    private static final int WIDTH_INDEX = 1;
    private static final int REPS_INDEX = 2;
    private static BufferedReader bfr;
    public static void init(){
        bfr = new BufferedReader(new InputStreamReader(System.in));
    }
    public static PricesParcel readPricesParcel() throws IOException {
        String[] dimensions = bfr.readLine().split(" ");
        int width = Integer.parseInt(dimensions[1]);
        int height = Integer.parseInt(dimensions[0]);
        PricesParcel pricesParcel = new PricesParcel(width, height);
        for (int y = 0; y < height; y++){
            Integer ints[] = parseIntegersFromLine(bfr.readLine(), width);
            int sum = 0;
            for (int x = 0; x < width; x++){
                sum+= ints[x];
                pricesParcel.prices[x][y] = ints[x];
                pricesParcel.linePrices[x][y] = sum;
            }
        }
        return pricesParcel;
    }
    private static Integer[] parseIntegersFromLine(String line, int width){
        Integer[] nums = new Integer[width];
        int index = 0;
        String buffer = "";
        for (char c : line.toCharArray()){
            if (c != ' '){
                buffer += c;
            }else if (buffer.length() != 0){
                nums[index] = Integer.parseInt(buffer);
                index++;
                buffer = "";
            }
        }
        if (buffer.length() != 0){
            nums[width - 1] = Integer.parseInt(buffer);
        }
        return nums;
    }

    public static CottageParcel[] readCottageParcels() throws IOException {
        int numberOfCottages = Integer.parseInt(bfr.readLine());
        CottageParcel[] cottages = new CottageParcel[numberOfCottages];
        for (int i = 0; i < numberOfCottages; i++){
            Integer[] numbers = parseIntegersFromLine(bfr.readLine(), 3);
            cottages[i] = new CottageParcel(numbers[WIDTH_INDEX], numbers[HEIGHT_INDEX], numbers[REPS_INDEX]);
        }
        Arrays.sort(cottages);
        return cottages;
    }

    public static void close() throws IOException {
        bfr.close();
    }


}
