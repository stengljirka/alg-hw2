package alg;

import java.util.Arrays;

public class CalculationData {
    int maxSize;
    int width, height, startY;
    int[] cottagesReps;
    boolean[] unusedBlocks;
    StartingPosition startingPosition;
    int[] startingXPositions;
    public CalculationData(CottageParcel[] cottages, int width, int height) {
        this.maxSize = 0;
        this.cottagesReps = new int[cottages.length];
        this.width = width;
        this.height = height;
        this.startY = 0;
        this.unusedBlocks = new boolean[width * height];
        this.startingXPositions = new int[height];
        Arrays.fill(startingXPositions, 0);
        Arrays.fill(unusedBlocks, true);
        for (int i = 0; i < cottages.length; i++){
            this.cottagesReps[i] = cottages[i].repetitons;
        }
    }

    public CalculationData(CalculationData data, StartingPosition startingPosition) {
        this.startingPosition = startingPosition;
        this.width = data.width;
        this.height = data.height;
        this.maxSize = data.maxSize;
        this.startY = data.startY;
        this.cottagesReps = new int[data.cottagesReps.length];
        this.unusedBlocks = new boolean[data.width * data.height];
        this.startingXPositions = new int[data.height];
        System.arraycopy(data.unusedBlocks, 0, this.unusedBlocks, 0, data.unusedBlocks.length);
        System.arraycopy(data.cottagesReps, 0, this.cottagesReps, 0, data.cottagesReps.length);
        System.arraycopy(data.startingXPositions, 0, this.startingXPositions, 0, data.startingXPositions.length);

    }

    @Override
    public String toString() {
//        String msg = "CalculationData{" +
//                "maxSize=" + maxSize +
//                ", width=" + width +
//                ", height=" + height +
//                ", cottages=" + Arrays.toString(cottagesReps) +
//                ", unusedBlocks=\n";
//        for (int y = 0; y < height; y++){
//            for (int x = 0; x < width; x++){
//                msg += unusedBlocks[x + y * width] + " ";
//            }
//            msg += "\n";
//        }
//
//        msg += Arrays.toString(startingXPositions);
        return Arrays.toString(this.cottagesReps);
    }

}
